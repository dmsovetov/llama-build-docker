FROM gcc:13.2.0

RUN apt-get update
RUN apt-get install -y cmake git

RUN git clone https://github.com/ggerganov/llama.cpp.git
RUN cd llama.cpp ; mkdir build ; cd build
RUN cd llama.cpp/build ; cmake -DBUILD_STATIC_LIBS=OFF -DLLAMA_BUILD_TESTS=OFF -DCMAKE_EXE_LINKER_FLAGS="-static" ..
RUN cd llama.cpp/build ; cmake --build . --config Release