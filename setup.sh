#!/bin/sh

docker build . -t llamacpp

id=$(docker create llamacpp)
docker cp $id:/llama.cpp/build/bin/perplexity .
docker rm -v $id

curl https://s3.amazonaws.com/research.metamind.io/wikitext/wikitext-2-raw-v1.zip?ref=salesforce-research -o wikitext.zip
unzip wikitext.zip