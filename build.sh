#!/bin/sh

git clone https://github.com/ggerganov/llama.cpp.git
cd llama.cpp ; mkdir build ; cd build
cd llama.cpp/build ; cmake -DBUILD_STATIC_LIBS=OFF -DLLAMA_BUILD_TESTS=OFF -DCMAKE_EXE_LINKER_FLAGS="-static" ..
cd llama.cpp/build ; cmake --build . --config Release